//
//  Recipe+CoreDataProperties.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//
//

import Foundation
import CoreData


extension Recipe {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Recipe> {
        return NSFetchRequest<Recipe>(entityName: "Recipe")
    }

    @NSManaged public var href: String?
    @NSManaged public var ingredients: String?
    @NSManaged public var thumbnail: NSURL?
    @NSManaged public var title: String?

}
