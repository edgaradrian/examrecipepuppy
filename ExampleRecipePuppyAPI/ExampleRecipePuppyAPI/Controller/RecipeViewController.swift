//
//  RecipeViewController.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit

class RecipeViewController: UIViewController, UITableViewDelegate {
    
    var recipePuppyStore: RecipePuppyStore!
    
    @IBOutlet var tableView: UITableView!
    let dataSource = RecipePuppyDataSource()
    
    var parametros: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Recipes"
        
        tableView.delegate = self
        tableView.dataSource = dataSource
        tableView.estimatedRowHeight = 105
        tableView.tableFooterView = UIView()
        
        uploadDataSource(params: parametros)
        
        recipePuppyStore.fetchRecipePuppy(params: parametros) { (RecipePuppyResult) in
            self.uploadDataSource(params: self.parametros)
        }
        
    }//viewDidLoad

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let recipe = dataSource.recipesStore[indexPath.row]
        
        recipePuppyStore.fetchImage(for: recipe) { (imageResult) in
            
            guard let index = self.dataSource.recipesStore.firstIndex(of: recipe), case let .success(image) = imageResult else {
                return
            }//guard
            
            let recipeIndexPath = IndexPath(item: index, section: 0)
            
            if let cell = self.tableView.cellForRow(at: recipeIndexPath) as? RecipePuppyTableViewCell {
                cell.update(with: image)
            }
            
        }
        
        
    }//willDisplay
    
    private func uploadDataSource(params: String?) {
        recipePuppyStore.fetchAllRecipePuppy(params: params) { (RecipePuppyResult) in
            
            do {
                self.dataSource.recipesStore = try RecipePuppyResult.get()
            }catch {
                self.dataSource.recipesStore.removeAll()
            }//catch
            
            self.tableView.reloadData()
        }
    }//uploadDataSource
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        switch segue.identifier {
        case "showRecipeDetail"?:
            if let row = tableView.indexPathForSelectedRow?.row {
                let recipe = dataSource.recipesStore[row]
                
                let destinationVC = segue.destination as! RecipeDetailViewController
                
                destinationVC.recipe = recipe
                destinationVC.store = recipePuppyStore
                
            }
        default:
            preconditionFailure("Unexpected segue identifier")
        }//
        
    }//prepare for segue
    
}//class RecipeViewController
