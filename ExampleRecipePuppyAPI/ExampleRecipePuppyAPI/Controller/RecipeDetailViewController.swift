//
//  RecipeDetailViewController.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit

class RecipeDetailViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var stackView: UIStackView!
    
    var store: RecipePuppyStore!
    var dataSource = RecipeImageDataSource()
    
    var recipe: Recipe! {
        didSet {
            navigationItem.title = recipe.title
            
        }//didSet
    }//recipe

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.dataSource = dataSource
        collectionView.delegate = self
        
        if let ingredientes = recipe.ingredients {
            print("ingredientes: \(ingredientes)")
            updateDateSource(ingredients: ingredientes)
        }
        

    }//ViewDidLoad
    
    //MARK: Delegate Flow Layout Methods
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = CGSize(width: 200, height: 200)
        return size
    }//sizeForItemAt
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1.0
    }
    
    
    @IBAction private func pressedLink() {
        performSegue(withIdentifier: "showWeb", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let url = "http://www.recipepuppy.com/about/api/"
        let identifier = segue.identifier
        
        switch identifier {
        case "showWeb"?:
            let destinationVC = segue.destination as! RecipeWebViewController
            destinationVC.url = URL(string: recipe.href ?? url)
        default:
            preconditionFailure("Unexpected segue identifier")
        }//switch
    
    }//prepare
    
    private func updateDateSource(ingredients: String) {
        
        let result = store.fetchImagesIngredients(for: ingredients)
        
        switch result {
            case let .success(recipeImages):
                dataSource.images = recipeImages
            case .failure:
                dataSource.images.removeAll()
        }
        
        self.collectionView.reloadData()
        
    }//updateDateSource
    
}//RecipeDetailViewController
