//
//  IndexViewController.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit

class IndexViewController: UIViewController, UITextFieldDelegate {
    
    var recipePuppyStore: RecipePuppyStore!
    
    @IBOutlet var textField: UITextField!
    
    //MARK: app methods
    override func viewDidLoad() {
        super.viewDidLoad()
        textField.delegate = self
        
    }//viewDidLoad
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    @IBAction func dismissKeyboard(_ sender: UITapGestureRecognizer) {
        textField.resignFirstResponder()
    }
    
    @IBAction func pressedGetRecipes(_ sender: UIButton) {
        view.window?.endEditing(true)
        performSegue(withIdentifier: "showRecipes", sender: self)
    }
    
    //MARK: - Text Field Delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.window?.endEditing(true)
        self.performSegue(withIdentifier: "showRecipes", sender: self)
        return true
    }//textFieldShouldReturn
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let existingTextHasColon = textField.text?.range(of: ":")
        let replacementTextHasColon = string.range(of: ":")
        
        if existingTextHasColon != nil, replacementTextHasColon != nil {
            return false
        } else {
            return true
        }
        
    }
    
    //MARK: Segue prepare
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
            case "showRecipes"?:
                let recipeViewController = segue.destination as! RecipeViewController
                recipeViewController.recipePuppyStore = recipePuppyStore
                if let params = textField.text {
                    recipeViewController.parametros = params
                }
            
            default:
                preconditionFailure("Unexpected segue identifier")
        }//switch
        
    }//prepare(for segue:)
    
}
