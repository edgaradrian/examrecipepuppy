//
//  RecipePuppyTableViewCell.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit

class RecipePuppyTableViewCell: UITableViewCell {
    
    @IBOutlet var labelTitle: UILabel!
    
    @IBOutlet var imageRecipe: UIImageView!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    func update(with image: UIImage?) {
        
        spinner.hidesWhenStopped = true
        
        if let imageToDisplay = image {
            spinner.stopAnimating()
            imageRecipe.image = imageToDisplay
        } else {
            spinner.startAnimating()
            imageRecipe.image = nil
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        update(with: nil)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        update(with: nil)
    }

}
