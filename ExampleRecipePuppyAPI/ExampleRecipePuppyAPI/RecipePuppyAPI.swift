//
//  RecipePuppyAPI.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import Foundation
import CoreData


enum RecipePuppyError: Error {
    case invalidJSONData
}

struct RecipePuppyAPI {
    
    private static let baseURL = "http://www.recipepuppy.com/api/"
    
    /*  get a recipe object from json and save it into the context
 
     - Parameter recipient: json array and the context
     
     - Returns: an optional recipe
     
        */
    private static func getRecipe(fromJSON json: [String:Any], into context: NSManagedObjectContext) -> Recipe? {
        
        guard let title = json["title"] as? String,
            let href = json["href"] as? String,
            let ingredients = json["ingredients"] as? String,
            let thumbnail = json["thumbnail"] as? String,
            let urlIMG = URL(string: thumbnail) else {
                return nil
        }
        
        
        
        let fetchRequest: NSFetchRequest<Recipe> = Recipe.fetchRequest()
        let predicate = NSPredicate(format: "title == %@", title)
        
        fetchRequest.predicate = predicate
        
        var fetchRecipe: [Recipe]?
        
        context.performAndWait {
            fetchRecipe = try? fetchRequest.execute()
        }
        
        if let recipe = fetchRecipe?.first {
            return recipe
        }
        
        var recipe: Recipe!
        
        context.performAndWait {
            recipe = Recipe(context: context)
            recipe.title = title
            recipe.href = href
            recipe.ingredients = ingredients
            recipe.thumbnail = urlIMG as NSURL
        }
        
        return recipe
        
    }
    
    /*  get a RecipePuppyResult (success or failure) from data
     
     - Parameter recipient: data and the context
     
     - Returns: a RecipePuppyResult
     
     */
    static func getRecipe(fromJSON data: Data, into context: NSManagedObjectContext) -> RecipePuppyResult {
        do {
            
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            
            guard let jsonDictionary = jsonObject as? [AnyHashable:Any], let results = jsonDictionary["results"] as? [[String:Any]] else {
                return .failure(RecipePuppyError.invalidJSONData)
            }
            
            var recipes = [Recipe]()
            
            for recipeResult in results {
                if let recipe = getRecipe(fromJSON: recipeResult, into: context) {
                    recipes.append(recipe)
                }
            }
            
            if recipes.isEmpty {
                return .failure(RecipePuppyError.invalidJSONData)
            }
            
            return .success(recipes)
            
        } catch let error {
            return .failure(error)
        }
        
    }//getRecipe
    
    /*  get a URL
     
     - Parameter recipient: params like recipe title and ingredients
     
     - Returns: a url
     
     */
    private static func getRecipePuppyURL(ingredients: String?, title: String?) -> URL {
        var componentes = URLComponents(string: baseURL)!
        var queryItems = [URLQueryItem]()
        
        let baseParams = [
            "i" : ingredients,
            "q" : title
        ]
        
        for (key, value) in baseParams {
            let item = URLQueryItem(name: key, value: value)
            queryItems.append(item)
        }
        
        componentes.queryItems = queryItems
        
        return componentes.url!
        
    }//getRecipePuppyURL
    
    
    static func getRecipePuppyURL(params: String?) -> URL {
        
        let tokens = params?.components(separatedBy: ":")
        
        return getRecipePuppyURL(ingredients: tokens?.last, title: tokens?.first)
        
    }//getRecipePuppyURL
    
    
}
