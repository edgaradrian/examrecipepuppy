//
//  RecipeImage.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit

class RecipeImage: Equatable {
    
    static func == (lhs: RecipeImage, rhs: RecipeImage) -> Bool {
        return lhs.idIngredient == rhs.idIngredient
    }
    
    var idIngredient: Int = Int(Date().timeIntervalSince1970)
    var nameIngredient: String
    var imageIngredient: UIImage?
    
    init(with name: String) {
        self.nameIngredient = name + "_label"
        self.imageIngredient = UIImage(named: name) ?? UIImage(named: "generic")
    }//init
    
}//RecipeImage
