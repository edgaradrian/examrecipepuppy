//
//  RecipeImageDataSource.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit

class RecipeImageDataSource: NSObject, UICollectionViewDataSource {
    
    var images = [RecipeImage]()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count
    }//numberOfItemsInSection
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let identifier = "viewCell"
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
        let imagen = images[indexPath.row].imageIngredient
        let imagenVista = UIImageView(image: imagen)
        imagenVista.frame = cell.contentView.frame
        imagenVista.contentMode = .scaleAspectFill
        
        cell.contentView.addSubview(imagenVista)
        
        
        return cell
    }//cellForItemAtindexPath
    
    
    
    
    
}//RecipeImageDataSource
