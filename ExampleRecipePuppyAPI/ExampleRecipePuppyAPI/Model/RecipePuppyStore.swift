//
//  RecipePuppyStore.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit
import CoreData

typealias RecipePuppyResult =  Result<[Recipe],Error>
typealias ImageResult = Result<UIImage,Error>
typealias IngredientResult = Result<[RecipeImage],Error>

enum ImageRecipeError: Error {
    case imageCreationError
}

enum IngredientError:Error {
    case imageError
}

class RecipePuppyStore {
    
    let imageStore = ImageStore()
    
    let session: URLSession = {
       let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    let persistentContainer: NSPersistentContainer = {
       let container = NSPersistentContainer(name: "Recipes")
        container.loadPersistentStores(completionHandler: { (description, error) in
            if let error = error {
                print("Error set up Core Data: \(error)")
            }
        })
        return container
    }()
    
    private func processingRecipePuppy(data: Data?, error: Error?) -> RecipePuppyResult {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return RecipePuppyAPI.getRecipe(fromJSON: jsonData, into: persistentContainer.viewContext)
    }//processingRecipePuppy
    
    func fetchRecipePuppy(params: String?, completion: @escaping (RecipePuppyResult) -> Void) {
        let url = RecipePuppyAPI.getRecipePuppyURL(params: params)
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) { (data, response, error) in
            
            var result = self.processingRecipePuppy(data: data, error: error)
            
            if case .success = result {
                do {
                    try self.persistentContainer.viewContext.save()
                } catch let error{
                    result = .failure(error)
                }//catch
            }//if case .success
            
            OperationQueue.main.addOperation {
                completion(result)
            }
            
        }
        
        task.resume()
        
    }//fetchRecipePuppy
    
    func fetchAllRecipePuppy(params: String?, completion: @escaping (RecipePuppyResult) -> Void) {
        
        var predicate: NSPredicate!
        
        let fetchRequest: NSFetchRequest<Recipe> = Recipe.fetchRequest()
        let sortDescriptor = NSSortDescriptor(key: #keyPath(Recipe.title), ascending: true)
        
        if let parametros = params?.components(separatedBy: ":"), let title = parametros.first, let ingredients = parametros.last {
            predicate = NSPredicate(format: "ingredients CONTAINS[c] %@ OR title CONTAINS[c] %@", ingredients, title)
            fetchRequest.predicate = predicate
        }
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let context = persistentContainer.viewContext
        
        context.perform {
            do {
                let allRecipes = try context.fetch(fetchRequest)
                completion(.success(allRecipes))
            } catch let error {
                completion(.failure(error))
            }
        }
        
    }//fetchAllRecipePuppy
    
    
    // MARK: - Image work
    
    func fetchImage(for recipe: Recipe, completion: @escaping (ImageResult) -> Void) {
        
        guard let title = recipe.title else {
            preconditionFailure("Recipe expected to have title")
        }
        
        if let image = imageStore.image(forKey: title) {
            OperationQueue.main.addOperation {
                completion(.success(image))
            }
            return
        }
        
        guard let imageURL = recipe.thumbnail else {
            preconditionFailure("Recipe without a remote URL.")
        }
        
        let request = URLRequest(url: imageURL as URL)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            let result = self.processImageRequest(data: data, error: error)
            
            if case let .success(image) = result {
                self.imageStore.setImage(image, forKey: title)
            }
            
            OperationQueue.main.addOperation {
                completion(result)
            }
        }
        
        task.resume()
   
    }
    
    private func processImageRequest(data: Data?, error: Error?) -> ImageResult {
        
        guard let imageData = data, let image = UIImage(data: imageData) else{
            
            if data == nil {
                return .failure(error!)
            } else {
                return .failure(ImageRecipeError.imageCreationError)
            }
            
        }
        
        return .success(image)
        
    }//processImageRequest
    
    //MARK: image ingredients
    func fetchImagesIngredients(for ingredients: String) -> IngredientResult {

        let recipeIngedients = ingredients.components(separatedBy: ",")
        
        var array = [RecipeImage]()
        
        for ingredient in recipeIngedients {
            let trimIngredient = ingredient.trimmingCharacters(in: .whitespaces)
            let recipeImage = RecipeImage(with: trimIngredient)
            array.append(recipeImage)
        }//ingredient
        
        if array.isEmpty {
            return .failure(IngredientError.imageError)
        }
        
        return .success(array)
        
    }//fetchImagesIngredients
    
    
}//RecipePuppyStore


