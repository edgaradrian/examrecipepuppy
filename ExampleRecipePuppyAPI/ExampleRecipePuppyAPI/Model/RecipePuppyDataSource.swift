//
//  RecipePuppyDataSource.swift
//  ExampleRecipePuppyAPI
//
//  Created by Edgar Adrián on 4/6/19.
//  Copyright © 2019 Edgar Adrián. All rights reserved.
//

import UIKit

class RecipePuppyDataSource: NSObject, UITableViewDataSource {
    
    var recipesStore = [Recipe]()
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipesStore.count
    }//numberOfRowsInSection
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let level = CGFloat(1.0) / CGFloat(recipesStore.count)
        
        let identifier = "UITableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! RecipePuppyTableViewCell
        
        let recipe = recipesStore[indexPath.row]
        cell.labelTitle.text = recipe.title
        cell.labelTitle.numberOfLines = 0
        cell.backgroundColor = UIColor.init(red: 35 / 255.0, green: 204/255.0, blue: 216.0/255.0, alpha: (level + CGFloat(indexPath.row) * level))
        return cell
    }
    
}//RecipePuppyDataSource
